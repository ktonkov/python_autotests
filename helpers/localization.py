import json
import io

LOCALIZATION_PATH = '../helpers/localization.json'

def get_localized_value(key, local):
    with io.open(LOCALIZATION_PATH, encoding='utf-8') as localization_file:
        data = json.load(localization_file)
    return data[local][key]
