import random, string

def random_email(name_length, domain_name_length, ending_length):
	letters = string.ascii_lowercase
	return ''.join(random.choice(letters) for i in range(name_length)) + '@' + ''.join(random.choice(letters) for i in range(domain_name_length)) + '.' + ''.join(random.choice(letters) for i in range(ending_length))

def random_password(length):
	letters = string.ascii_lowercase
	return ''.join(random.choice(letters) for i in range(length))
