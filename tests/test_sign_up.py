import pytest
from pages.sign_in import SignInPage
from pages.sign_up import SignUpPage
from pages.wizard import Wizard
from pages.navigation_bar import NavigationBar
from objects.user import User
from helpers.localization import get_localized_value
from helpers.random_string_generators import random_email, random_password

@pytest.mark.parametrize(
    'user_local',
	['EN']
)
def test_sign_up(browser, user_local):
	USER = User((random_email(10, 8, 3), random_password(12), user_local))

	browser.maximize_window()

	sign_in_page = SignInPage(browser)
	sign_in_page.load()
	sign_in_page.sign_up()

	sign_up_page = SignUpPage(browser)

	sign_up_page.mouse_over_email_input()
	assert sign_up_page.is_email_tooltip_displayed()
	sign_up_page.enter_email(USER.email)
	sign_up_page.mouse_over_password_input()
	#assert sign_up_page.is_password_tooltip_displayed()
	sign_up_page.enter_password(USER.password)
	sign_up_page.mouse_over_repeat_password_input()
	assert sign_up_page.is_repeat_password_tooltip_displayed()
	sign_up_page.enter_repeat_password(USER.password)
	sign_up_page.sign_up()

	wizard = Wizard(browser)
	assert wizard.is_active()
	wizard.skip()
	assert not wizard.is_active()

	navigation_bar = NavigationBar(browser)
	assert navigation_bar.get_active_nav_link_text() == get_localized_value('templates_nav_link', USER.local)
	assert navigation_bar.get_user_drop_down_letter() == USER.email[0].upper()
	assert navigation_bar.get_user_drop_down_header() == USER.email
