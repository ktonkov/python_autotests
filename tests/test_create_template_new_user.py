import pytest
import os

from pages.sign_in import SignInPage
from pages.sign_up import SignUpPage
from pages.wizard import Wizard
from pages.templates import TemplatesPage
from pages.template_creation import TemplateCreationPage

from objects.user import User
from objects.document import Document
from objects.template import Template

from helpers.random_string_generators import random_email, random_password

def test_create_template_new_user(browser):
    USER = User((random_email(10, 8, 3), random_password(12), 'EN'))
    DOCUMENT = Document(os.path.abspath('../data_samples/hs/photo_2020-04-08_21-11-48.jpg'), {'width': 989, 'height': 1280})
    TEMPLATE = Template('Test Template', DOCUMENT, None, [{"name":"Name","lang":"eng","x":151.02619269907785,"y":150.82681960207384,"width":213.84014454541284,"height":24.375560518129987}])
    PATH = os.path.abspath('../data_samples/hs/photo_2020-04-08_21-11-48.jpg')

    browser.maximize_window()

    sign_in_page = SignInPage(browser)
    sign_in_page.load()
    sign_in_page.sign_up()

    sign_up_page = SignUpPage(browser)
    sign_up_page.sign_up(USER)

    wizard = Wizard(browser)
    wizard.skip()

    templates_page = TemplatesPage(browser)
    templates_page.upload_file(DOCUMENT)

    template_creation_page = TemplateCreationPage(browser)

    template_creation_page.save_template(TEMPLATE)

    assert template_creation_page.is_template_addad(TEMPLATE)
