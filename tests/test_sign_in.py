import pytest
from pages.sign_in import SignInPage
from pages.wizard import Wizard
from pages.navigation_bar import NavigationBar
from objects.user import User
from helpers.localization import get_localized_value

@pytest.mark.parametrize(
    'user',
    [('autotestEnUser@test.com', 'test', 'EN')]
)
def test_sign_in(browser, user):
    USER = User(user)

    browser.maximize_window()

    sign_in_page = SignInPage(browser)
    sign_in_page.load()
    sign_in_page.sign_in(USER)

    wizard = Wizard(browser)
    assert wizard.is_active()
    wizard.skip()
    assert not wizard.is_active()

    navigation_bar = NavigationBar(browser)
    assert navigation_bar.get_active_nav_link_text() == get_localized_value('templates_nav_link', USER.local)
    assert navigation_bar.get_user_drop_down_letter() == USER.email[0].upper()
    assert navigation_bar.get_user_drop_down_header() == USER.email
