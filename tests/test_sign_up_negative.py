import pytest
import random, string
from pages.sign_in import SignInPage
from pages.sign_up import SignUpPage
from pages.wizard import Wizard
from pages.navigation_bar import NavigationBar
from objects.user import User
from helpers.localization import get_localized_value
from helpers.random_string_generators import random_email, random_password

@pytest.mark.parametrize(
    'user',
	[('', '', 'EN'), ('abcd', 'абвг', 'EN'), ('abcd@abcd', 'abc!', 'EN'), ('abcd.abcd', 'abc!', 'EN'), ('abcd.abcd@abcd', 'abc!', 'EN')]
)
def test_sign_up_negative(browser, user):
	USER = User(user)

	browser.maximize_window()

	sign_in_page = SignInPage(browser)
	sign_in_page.load()
	sign_in_page.sign_up()

	sign_up_page = SignUpPage(browser)

	#sign_up_page.sign_up(USER)
	sign_up_page.enter_email(USER.email)
	assert sign_up_page.is_email_invalid_feedback_displayed()
	sign_up_page.enter_password(USER.password)
	assert sign_up_page.is_password_invalid_feedback_displayed()
	sign_up_page.enter_repeat_password(USER.password)
	assert sign_up_page.is_repeat_password_invalid_feedback_displayed()
	sign_up_page.sign_up()
	assert sign_up_page.is_email_invalid_feedback_displayed()
	assert sign_up_page.is_password_invalid_feedback_displayed()
	assert sign_up_page.is_repeat_password_invalid_feedback_displayed()
