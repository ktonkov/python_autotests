from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

class SignInPage:
    URL = 'https://qa.parsio.app/signin'

    EMAIL_INPUT = (By.ID, 'email')
    PASSWORD_INPUT = (By.ID, 'password')

    SIGN_IN_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"].sign-form__button.btn.btn-primary.btn-sm')
    SIGN_UP_BUTTON = (By.CSS_SELECTOR, '.mr-3.sign-form__button.btn.btn-link.btn-sm')

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)

    def sign_in(self, user):
        email_input = self.browser.find_element(*self.EMAIL_INPUT)
        email_input.send_keys(user.email)
        email_input = self.browser.find_element(*self.PASSWORD_INPUT)
        email_input.send_keys(user.password)
        sign_in_button = self.browser.find_element(*self.SIGN_IN_BUTTON)
        sign_in_button.click()

    def sign_up(self):
        sign_up_button = self.browser.find_element(*self.SIGN_UP_BUTTON)
        sign_up_button.click()
