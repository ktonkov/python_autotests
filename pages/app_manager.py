from selenium.webdriver.common.by import By

from sign_in import SignInPage


class AppManager:
    URL = 'https://qa.parsio.app/signin'

    EMAIL_INPUT = (By.ID, 'email')
    PASSWORD_INPUT = (By.ID, 'password')

    SIGN_IN_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"].sign-form__button.btn.btn-primary.btn-sm')
    SIGN_UP_BUTTON = (By.CSS_SELECTOR, '.mr-2.sign-form__button.btn.btn-link.btn-sm')

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)
        return SignInPage(self.browser)

    def go_to(self):
        return self
