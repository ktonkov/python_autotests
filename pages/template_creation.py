import math

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.webdriver.common.action_chains import ActionChains

class TemplateCreationPage:
    CANVAS = (By.CSS_SELECTOR, 'canvas')
    DOCUMENT_RATIO_CONTROL = (By.CSS_SELECTOR, '.text-center.body-regular.cursor-pointer.form-control.size-tumbler-info')
    MARK_NAME_INPUT = (By.ID, 'name')
    ADD_MARK_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"].btn.btn-primary.btn-sm')
    TEMPLATE_NAME_INPUT = (By.CSS_SELECTOR, '.p-2.header-bold-2.input-component__input')
    SAVE_TEMPLATE_BUTTON = (By.CSS_SELECTOR, '.mr-2.btn.btn-primary.btn-sm')

    def __init__(self, browser):
        self.browser = browser

    def is_canvas_displayed(self):
        canvas = self.browser.find_elements(*self.CANVAS)
        return len(canvas) > 0

    def get_canvas_location(self):
        canvas = self.browser.find_element(*self.CANVAS)
        canvas_location = canvas.location
        return canvas_location

    def get_canvas_size(self):
        canvas = self.browser.find_element(*self.CANVAS)
        canvas_size = canvas.size
        return canvas_size

    def get_canvas_center(self):
        canvas_location = self.get_canvas_location()
        canvas_size = self.get_canvas_size()
        canvas_center = {'x': canvas_location['x'] + canvas_size['width']/2, 'y': canvas_location['y'] + canvas_size['height']/2}
        return canvas_center

    def get_document_ratio(self):
        document_ratio_control = self.browser.find_element(*self.DOCUMENT_RATIO_CONTROL)
        document_ratio = int(document_ratio_control.get_attribute('textContent')[:-1])
        return document_ratio

    def get_document_location(self, document):
        canvas_center = self.get_canvas_center()
        document_ratio = self.get_document_ratio()
        document_location = {'x': math.ceil(canvas_center['x'] - document.size['width']*document_ratio/100/2),
                             'y': math.ceil(canvas_center['y'] - document.size['height']*document_ratio/100/2)}
        return document_location

    def get_offset_x_from_canvas_center(self, region, document):
        document_ratio = self.get_document_ratio()
        return -1*document.size['width']*document_ratio/100/2 + region['x']*document_ratio/100

    def get_offset_y_from_canvas_center(self, region, document):
        document_ratio = self.get_document_ratio()
        return -1*document.size['height']*document_ratio/100/2 + region['y']*document_ratio/100

    def draw_region(self, region, document):
        canvas = self.browser.find_element(*self.CANVAS)
        canvas_location = canvas.location
        document_ratio = self.get_document_ratio()
        ac = ActionChains(self.browser)
        ac.move_to_element(canvas) \
          .move_by_offset(self.get_offset_x_from_canvas_center(region, document), self.get_offset_y_from_canvas_center(region, document)) \
          .click_and_hold() \
          .move_by_offset(region['width']*document_ratio/100, region['height']*document_ratio/100) \
          .release() \
          .perform()

    def add_mark(self, region, document):
        self.draw_region(region, document)
        mark_name_input = self.browser.find_element(*self.MARK_NAME_INPUT)
        mark_name_input.send_keys(region['name'])
        add_murk_button = self.browser.find_element(*self.ADD_MARK_BUTTON)
        add_murk_button.click()

    def add_marks(self, template):
        for region in template.regions:
            self.add_mark(region, template.document)

    def save_template(self, template=None):
        if template != None:
            self.add_marks(template)
            template_name_input = self.browser.find_element(*self.TEMPLATE_NAME_INPUT)
            ac = ActionChains(self.browser)
            ac.double_click(template_name_input).perform()
            template_name_input.clear()
            template_name_input.send_keys(template.name)
        save_template_button = self.browser.find_element(*self.SAVE_TEMPLATE_BUTTON)
        save_template_button.click()
