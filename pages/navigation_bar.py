from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.common.exceptions import NoSuchElementException

class NavigationBar:
    USER_DROPDOWN_LETTER = (By.CSS_SELECTOR, '.user-dropdown__info.mr-2')
    USER_DROPDOWN_HEADER = (By.CSS_SELECTOR, '.list__header.dropdown-header')

    ACTIVE_NAV_LINK = (By.CSS_SELECTOR, '.nav-link.active')

    def __init__(self, browser):
        self.browser = browser

    def get_active_nav_link_text(self):
        active_nav_link = self.browser.find_element(*self.ACTIVE_NAV_LINK)
        return active_nav_link.get_attribute('textContent')

    def get_user_drop_down_letter(self):
        user_drop_down_letter = self.browser.find_element(*self.USER_DROPDOWN_LETTER)
        return user_drop_down_letter.get_attribute('textContent')

    def get_user_drop_down_header(self):
        user_drop_down_header = self.browser.find_element(*self.USER_DROPDOWN_HEADER)
        return user_drop_down_header.get_attribute('textContent')
