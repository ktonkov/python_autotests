from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.common.exceptions import NoSuchElementException

class Wizard:
    SKIP_WIZARD_BUTON = (By.CLASS_NAME, 'wizard-close-icon')

    def __init__(self, browser):
        self.browser = browser

    def is_active(self):
        skip_wizard_button = self.browser.find_elements(*self.SKIP_WIZARD_BUTON)
        return len(skip_wizard_button) > 0

    def skip(self):
        skip_wizard_button = self.browser.find_element(*self.SKIP_WIZARD_BUTON)
        skip_wizard_button.click()
