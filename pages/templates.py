from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

from selenium.common.exceptions import NoSuchElementException

class TemplatesPage:
    NEW_TEMPLATE_BUTTON = (By.CSS_SELECTOR, '#refWrapper[class="templates__button"] > .btn.btn-primary.btn-sm')
    FILE_INPUT = (By.CSS_SELECTOR, '.create-template-component__input')
    UPLOAD_FILE_DONE_BUTTON = (By.CSS_SELECTOR, '#refWrapper[class="done-button-wrapper mr-2"] > .btn.btn-primary.btn-sm')
    TAMPLATES_ROWS = (By.CSS_SELECTOR, 'tr.list-view__row')
    TAMPLATE_NAME_CELL = (By.CSS_SELECTOR, '.body-regular.no-active')

    def __init__(self, browser):
        self.browser = browser

    def get_active_nav_link_text(self):
        active_nav_link = self.browser.find_element(*self.ACTIVE_NAV_LINK)
        return active_nav_link.get_attribute('textContent')

    def upload_file(self, document):
        try:
            new_template_button = self.browser.find_element(*self.NEW_TEMPLATE_BUTTON)
            new_template_button.click()
        except NoSuchElementException:
            pass
        file_input = self.browser.find_element(*self.FILE_INPUT)
        file_input.send_keys(document.path)
        upload_file_done_button = self.browser.find_element(*self.UPLOAD_FILE_DONE_BUTTON)
        upload_file_done_button.click()

    def get_template_row(seld, index):
        tamplates_rows = self.browser.find_elements(*self.TAMPLATES_ROWS)
        return tamplates_rows[index]
    
    def is_template_addad(self, template):
        tamplate_row = self.get_template_row(0)
        tamplate_name_cell = tamplate_row.find_element(*self.TAMPLATE_NAME_CELL)
        return tamplate_name_cell.get_attribute['textContent'] == template.name
