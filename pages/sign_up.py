from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

class SignUpPage:
    URL = 'https://qa.parsio.app/signup'

    EMAIL_INPUT = (By.NAME, 'email')
    PASSWORD_INPUT = (By.NAME, 'password1')
    REPEAT_PASSWORD_INPUT = (By.NAME, 'password2')

    INVALID_FEEDBACK = (By.CSS_SELECTOR, '.invalid-feedback')
    TOOLTIP_TEXT = (By.CSS_SELECTOR, '.tooltip__text')
    SIGN_UP_BUTTON = (By.CSS_SELECTOR, 'button[type="submit"].sign-form__button.btn.btn-primary.btn-sm')

    def __init__(self, browser):
        self.browser = browser

    def load(self):
        self.browser.get(self.URL)

    def sign_up(self, user=None):
        if user != None:
            email_input = self.browser.find_element(*self.EMAIL_INPUT)
            email_input.send_keys(user.email)
            password_input = self.browser.find_element(*self.PASSWORD_INPUT)
            password_input.send_keys(user.password)
            repeat_password_input = self.browser.find_element(*self.REPEAT_PASSWORD_INPUT)
            repeat_password_input.send_keys(user.password)
        sign_in_button = self.browser.find_element(*self.SIGN_UP_BUTTON)
        sign_in_button.click()

    def enter_email(self, email):
        email_input = self.browser.find_element(*self.EMAIL_INPUT)
        email_input.send_keys(email)

    def enter_password(self, password):
        password_input = self.browser.find_element(*self.PASSWORD_INPUT)
        password_input.send_keys(password)

    def enter_repeat_password(self, repeat_password):
        repeat_password_input = self.browser.find_element(*self.REPEAT_PASSWORD_INPUT)
        repeat_password_input.send_keys(repeat_password)

    def mouse_over_email_input(self):
        email_input = self.browser.find_element(*self.EMAIL_INPUT)
        hover = ActionChains(self.browser).move_to_element(email_input)
        hover.perform()

    def mouse_over_password_input(self):
        password_input = self.browser.find_element(*self.PASSWORD_INPUT)
        hover = ActionChains(self.browser).move_to_element(password_input)
        hover.perform()

    def mouse_over_repeat_password_input(self):
        repeat_password_input = self.browser.find_element(*self.REPEAT_PASSWORD_INPUT)
        hover = ActionChains(self.browser).move_to_element(repeat_password_input)
        hover.perform()

    def is_email_invalid_feedback_displayed(self):
        email_input = self.browser.find_element(*self.EMAIL_INPUT)
        tooltip_container = email_input.parent
        invalid_feedback = tooltip_container.find_element(*self.INVALID_FEEDBACK)
        return invalid_feedback.get_attribute('textContent') != ''

    def get_email_invalid_feedback_text(self):
        email_input = self.browser.find_element(*self.EMAIL_INPUT)
        tooltip_container = email_input.parent
        invalid_feedback = tooltip_container.find_element(*self.INVALID_FEEDBACK)
        return invalid_feedback.get_attribute('textContent')

    def is_password_invalid_feedback_displayed(self):
        password_input = self.browser.find_element(*self.PASSWORD_INPUT)
        tooltip_container = password_input.parent
        invalid_feedback = tooltip_container.find_element(*self.INVALID_FEEDBACK)
        return invalid_feedback.get_attribute('textContent') != ''

    def get_password_invalid_feedback_text(self):
        password_input = self.browser.find_element(*self.PASSWORD_INPUT)
        tooltip_container = password_input.parent
        invalid_feedback = tooltip_container.find_element(*self.INVALID_FEEDBACK)
        return invalid_feedback.get_attribute('textContent')

    def is_repeat_password_invalid_feedback_displayed(self):
        repeat_password_input = self.browser.find_element(*self.REPEAT_PASSWORD_INPUT)
        tooltip_container = repeat_password_input.parent
        invalid_feedback = tooltip_container.find_element(*self.INVALID_FEEDBACK)
        return invalid_feedback.get_attribute('textContent') != ''

    def get_repeat_password_invalid_feedback_text(self):
        repeat_password_input = self.browser.find_element(*self.REPEAT_PASSWORD_INPUT)
        tooltip_container = repeat_password_input.parent
        invalid_feedback = tooltip_container.find_element(*self.INVALID_FEEDBACK)
        return invalid_feedback.get_attribute('textContent')

    def is_email_tooltip_displayed(self):
        email_input = self.browser.find_element(*self.EMAIL_INPUT)
        tooltip_container = email_input.parent
        tooltip_text = tooltip_container.find_element(*self.TOOLTIP_TEXT)
        return tooltip_text.is_displayed()

    def is_password_tooltip_displayed(self):
        password_input = self.browser.find_element(*self.PASSWORD_INPUT)
        tooltip_container = password_input.parent
        tooltip_text = tooltip_container.find_element(*self.TOOLTIP_TEXT)
        return tooltip_text.is_displayed()

    def is_repeat_password_tooltip_displayed(self):
        repeat_password_input = self.browser.find_element(*self.REPEAT_PASSWORD_INPUT)
        tooltip_container = repeat_password_input.parent
        tooltip_text = tooltip_container.find_element(*self.TOOLTIP_TEXT)
        return tooltip_text.is_displayed()
